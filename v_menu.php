<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
    <?php $level = $this->session->userdata('level'); ?>
    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center"
        href="<?= site_url('template/')?>index.html">
        <div class="sidebar-brand-icon">
            <i class="fas fa-university"></i>
        </div>
        <div class="sidebar-brand-text mx-3">MONITORING MAGANG<sup></sup></div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item active">
        <a class="nav-link" href="<?= site_url('home')?>">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">
    <li class="nav-item">
    <?php if (in_array($level, array(7,0,19))):?>
        <a class="nav-link collapsed" href="<?= site_url('mahasiswa')?>" aria-expanded="true" aria-controls="collapseTwo3">
            <i class="fas fa-fw fa-user"></i>
            <span>Daftar Magang Mahasiswa</span>
        </a>
        <?php endif;?>
        <!-- <div id="collapseTwoBK" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header"></h6>
                <a class="collapse-item" href="<?= site_url('mahasiswa')?>">DATA</a>
                <?php if (in_array($level, array(7))):?>
                <a class="collapse-item" href="<?= site_url('mahasiswa/tambah_mahasiswa')?>">TAMBAH</a>
                <?php endif;?>
            </div>
        </div> -->

    </li>
    
    <li class="nav-item">
    <!-- <?php if (in_array($level, array(8,7))):?>
        <a class="nav-link collapsed" href="<?= site_url('nilai')?>" aria-expanded="true" aria-controls="collapseTwo3">
            <i class="fas fa-fw fa-file"></i>
            <span>NILAI</span>
        </a> -->
        <!-- <?php endif;?> -->
        <!-- <div id="collapseTwoBY" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="<?= site_url('nilai')?>">DATA</a>
                <?php if (in_array($level, array(8,7))):?>
                <a class="collapse-item" href="<?= site_url('nilai/tambah_nilai')?>">Input</a>
                <?php endif;?>
            </div>
        </div> -->
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href="<?= site_url('jurnal')?>" aria-expanded="true" aria-controls="collapseTwo3">
            <i class="fas fa-fw fa-folder"></i>
            <span>Data Jurnal Harian</span>
        </a>
    </li>

    

   
    

    <!-- <li class="nav-item">
        <a class="nav-link collapsed" href="<?= site_url('template/')?>#" data-toggle="collapse"
            data-target="#pembimbing" aria-expanded="true" aria-controls="collapseTwo3">
            <i class="fas fa-fw fa-cog"></i>
            <span>PEMBIMBING</span>
        </a>
        <div id="pembimbing" class="collapse" aria-labelledby="headingTwo" data-parent="#pembimbing">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="<?= site_url('pembimbing')?>">DATA</a>
                <a class="collapse-item" href="<?= site_url('pembimbing/tambah_pembimbing')?>">TAMBAH</a>
            </div>
        </div>
    </li> -->

    <!-- <li class="nav-item">
        <a class="nav-link collapsed" href="<?= site_url('template/')?>#" data-toggle="collapse"
            data-target="#collapseTwoS" aria-expanded="true" aria-controls="collapseTwo3">
            <i class="fas fa-fw fa-cog"></i>
            <span>USER</span>
        </a>
        <div id="collapseTwoS" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header"></h6>
                <a class="collapse-item" href="<?= site_url('users')?>">DATA</a>
                <a class="collapse-item" href="<?= site_url('users/tambah_users')?>">TAMBAH</a>
            </div>
        </div>
    </li> -->

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>


</ul>